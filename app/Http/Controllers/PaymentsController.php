<?php namespace App\Http\Controllers;

use App\Payment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PaymentsController extends Controller {


    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $stripe = array(
            'secret_key'      => getenv('secret_key'),
            'publishable_key' => getenv('publishable_key')
        );
        $payment_info=array("amount"   => 1000, "currency" => "cad");

        \Stripe\Stripe::setApiKey($stripe['secret_key']);

        $attributes = $request->all();
        $token = $attributes['stripeToken'];

        $customer = \Stripe\Customer::create(array(
                "source" => $token,
                "description" => "Example customer")
        );
        try
        {
            $charge = \Stripe\Charge::create(array(
                    "customer" => $customer->id,
                    "amount"   => $payment_info["amount"], // 10 cad$
                    "currency" => $payment_info["currency"]
                )
            );
        } catch (\Stripe\Error\Card $e)
        {
            // The card has been declined
            flash()->error('Payment failed!');
            return redirect('/card');
        }

        //store in db
        $dataToStore=array(
            "customer" =>$customer->id,
            'amount'=>$payment_info["amount"],
            'currency'=>$payment_info["currency"]
        );
        $payment = Payment::create($dataToStore);
        flash()->success('Payment success!');
        return redirect('/card');
    }

}