<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script type="text/javascript">
        Stripe.setPublishableKey("<?php echo getenv('publishable_key'); ?>");
    </script>

</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        @include('flash::message')

        <form action="card" method="POST" id="payment-form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <span class="payment-errors"></span>

            <div class="form-row">
                <label>
                    <span>Card Number</span>
                    <input type="text" size="20" id="number" data-stripe="number">
                </label>
            </div>

            <div class="form-row">
                <label>
                    <span>Expiration (MM/YY)</span>
                    <input type="text" size="2" id="exp_month" data-stripe="exp_month">
                </label>
                <span> / </span>
                <input type="text" size="2" id="exp_year" data-stripe="exp_year">
            </div>

            <div class="form-row">
                <label>
                    <span>CVC</span>
                    <input type="text" size="4" id="cvc" data-stripe="cvc">
                </label>
            </div>

            <button type="submit" class="arrow">Submit</button>
        </form>

        <p id="payment-result"></p>

        <script>
            //4242 4242 4242 4242


            var $form = $('#payment-form');
            var $result = $('#payment-result');

            $(function() {
                $form.submit(function(event) {
                    event.preventDefault();

                    $form.find('.errors').text('');
                    $form.find('.submit').prop('disabled', true);
                    if (Stripe.card.validateCardNumber($('#number').val())
                            && Stripe.card.validateExpiry($('#exp_month').val(), $('#exp_year').val())
                            && Stripe.card.validateCVC($('#cvc').val())) {

                        Stripe.card.createToken($form, stripeResponseHandler);
                    } else {
                        $result.html('Some fields invalid!');

                        $form.find('.errors').text('Please enter valid test credit card information.');
                        $form.find('.submit').prop('disabled', false);
                    }

                    return false;
                });
            });
            function stripeResponseHandler(status, response) {
                if (response.error) {
                    $form.find('.errors').text(response.error.message);
                    $form.find('.submit').prop('disabled', false);
                } else {
                    $result.html('Card is valid. Charging to it ...');

                    $form.append("<input type='hidden' name='stripeToken' value='" + response.id + "' />");
                    $form.get(0).submit();
                }
            };
        </script>
    </div>
</div>
</body>
</html>
